COLOR CONSTANCY GENERALIZED IN THE IMAGE PROCESSING
===========

This codes implement the function that applies the constancy color algorithms know
as White Patch, Gray World and Gray Edges, offering to apply the variation of method
Finlayson & Trezzi and allowing smoothing to be applied by gaussian filter, selectec
by the user.Finally the function offers the option of to apply a variation to each
method where yo want to maximize the contrast imagen through of segmented expansion
of histogram. Also, the code show the result of to apply a measure of evaluation
of the chromatic content of the images.
More in: http://www.jovenesenlaciencia.ugto.mx/index.php/jovenesenlaciencia/article/download/1900/1401
----

Authors
-----------
Henry Areiza,
Fernando Pamplona,
Joaquin Guajo
Created: 4 July of 2017

Actualized: 9 March of 2018



Requeriments of software
----------
El c�digo requiere contar con las librer�as de OpenCV 2.4.12
La informaci�n te�rica de los m�todos se encuentra en el siguiente art�culo:
FUNDAMENTOS DEL PROCESAMIENTO DE IM�GENES NOCTURNAS MEDIANTE EL AUMENTO DEL RANGO DIN�MICO
 http://www.jovenesenlaciencia.ugto.mx/index.php/jovenesenlaciencia/article/download/1900/1401


Linux OpenCV Installation and code execution guide from http://www.codebind.com/cpp-tutorial/install-opencv-ubuntu-cpp/
----

Windows OpenCV Installation
----
-> Codebloks from https://kevinhughes.ca/tutorials/opencv-install-on-windows-with-codeblocks-and-mingw
----
-> Visual estudio from http://korud.com/blog/instalar-opencv-3-1-y-visual-studio-2015/
----
 
