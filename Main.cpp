/// COLOR CONSTANCY GENERALIZED
/// Henry Jho�n Areiza Laverde
/// Creado el 04 de julio de 2017
/// Actualizado el 09 de marzo de 2018
/// Este segmento de c�digo implementa una funci�n que aplica los algoritmos de
/// constancia de color conocidos como White Patch, Gray World y Gray Edges,
/// ofreciendo aplicar la variaci�n del m�todo de Finlayson & Trezzi y permitiendo
/// tambi�n que se aplique un suavizado por filtro Gaussiano a selecci�n del
/// usuario. Finalmente la funci�n tambi�n ofrece la opci�n de aplicar una variaci�n
/// a cada m�todo en donde se busca maximizar el contraste de la imagen a trav�s de una
/// expansi�n segmentada del histograma, el c�digo muestra tambi�n el resultado de
/// de aplicar una medida de evaluaci�n del contenido crom�tico de las im�genes.

/// El c�digo requiere contar con las librer�as de OpenCV 2.4.12
/// La informaci�n te�rica de los m�todos se encuentra en el siguiente art�culo:
/// FUNDAMENTOS DEL PROCESAMIENTO DE IM�GENES NOCTURNAS MEDIANTE EL AUMENTO DEL RANGO DIN�MICO
/// http://www.jovenesenlaciencia.ugto.mx/index.php/jovenesenlaciencia/article/download/1900/1401

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include "iostream"
#include "math.h"
#include "ColorConstancy.h"
#include "stdlib.h" // biblioteca para usar borrado de pantalla


using namespace cv;
using namespace std;

int main(){

	Mat Imagen;
<<<<<<< HEAD
        Imagen = imread("/home/joaquin/color_constancy/Imagenes/Ejm_1.png");
        if(! Imagen.data ) {
        std::cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }
 
		  		
    
    ColorConstancy Prueba(Imagen);

    Prueba.White_patch(5, 0);
    waitKey();
    Prueba.Gray_World(5, 1);
    waitKey();
    Prueba.Shades_of_Gray(6, 5, 0);
    waitKey();
    Prueba.Gray_Edges(1, 5, 1);
    waitKey();
    Prueba.Gray_Edges(2, 5, 0);
=======
	string PathIM, NomGuard;
	int Opcion = 0;
	float Sigma, Mink;
	int Edges;
	bool Modi;

    cout << "\nBIENVENIDO!!\n\nEste programa esta disenado para aplicar\n"
         << "algunos algoritmos de constancia de color.\n\n";

    cout << "\nIngrese la direccion de la imagen a procesar.\n"
                 << "Ejemplo: C:\\\\Users\\\\NombreEquipo\\\\Imagenes\\\\_NombreImagen.png\n"
                 << "Direccion: ";
            cin >> PathIM; //  C:\\Users\\Ivancho\\Dropbox\\7_Investigacion\\Mexico\\Imagenes\\Ejm_2.png
            Imagen = imread(PathIM);
            ColorConstancy ImagenCC(Imagen);
            system ("cls");
            cout << "\t\nImagen leida correctamente!!\n\n";

	do{
		cout << "Por favor ingrese una de las siguientes opciones:\n\n"
             << "\t1. Aplicar 'White Patch'.\n"
             << "\t2. Aplicar 'Gray World'.\n"
             << "\t3. Aplicar 'Shades of Gray'.\n"
             << "\t4. Aplicar 'Gray Edges'.\n"
             << "\t5. Guardar imagen modificada.\n"
             << "\t6. Salir del programa.\n"
             << "\n\tOpcion: ";

        cin >> Opcion;

	    if(Opcion < 1 || Opcion > 6){
            system ("cls"); // borramos la pantalla
            cout << endl << "La opcion ingresada no es valida!! =("
                 << endl << "Por favor intentelo de nuevo."<< endl << endl << endl;
		} else {
		    switch(Opcion){
                case 1:{
                    system ("cls");
                    cout << "\nIngrese el sigma: ";
                    cin >> Sigma;
                    cout << "\nIngrese si es modificado (0, 1): ";
                    cin >> Modi;
                    cout << "\n\n\t Medidas de evaluacion:\n";
                    ImagenCC.White_patch(Sigma, Modi);
                    waitKey(100);
                    cout << "\n\n\n";
                    system("pause");
                    system ("cls");
                    break;
                }
                case 2:{
                    system ("cls");
                    cout << "\nIngrese el sigma: ";
                    cin >> Sigma;
                    cout << "\nIngrese si es modificado (0, 1): ";
                    cin >> Modi;
                    cout << "\n\n\t Medidas de evaluacion:\n";
                    ImagenCC.Gray_World(Sigma, Modi);
                    waitKey(100);
                    cout << "\n\n\n";
                    system("pause");
                    system ("cls");
                    break;
                }
                case 3:{
                    system ("cls");
                    cout << "\nIngrese el grado de la norma: ";
                    cin >> Mink;
                    cout << "\nIngrese el sigma: ";
                    cin >> Sigma;
                    cout << "\nIngrese si es modificado (0, 1): ";
                    cin >> Modi;
                    cout << "\n\n\t Medidas de evaluacion:\n";
                    ImagenCC.Shades_of_Gray(Mink, Sigma, Modi);
                    waitKey(100);
                    cout << "\n\n\n";
                    system("pause");
                    system ("cls");
                    break;
                }
                case 4:{
                    system ("cls");
                    cout << "\nIngrese la derivada: ";
                    cin >> Edges;
                    cout << "\nIngrese el sigma: ";
                    cin >> Sigma;
                    cout << "\nIngrese si es modificado (0, 1): ";
                    cin >> Modi;
                    cout << "\n\n\t Medidas de evaluacion:\n";
                    ImagenCC.Gray_Edges(Edges, Mink, Modi);
                    waitKey(100);
                    cout << "\n\n\n";
                    system("pause");
                    system ("cls");
                    break;
                }
                case 5:{
                    system ("cls");
                    cout << "\nIngrese el nombre que desea poner a la imagen\n"
                         << "con su respectiva extension y sin espacios:";
                    cin >> NomGuard;
                    ImagenCC.Guardar_Imagen(NomGuard);
                    waitKey(100);
                    cout << "\n\n\n";
                    system("pause");
                    system ("cls");
                    cout << "\t\nImagen guardada correctamente!!\n\n";
                    break;
                }
            }
		}
	}while (Opcion != 6);
>>>>>>> 7159bbc1dde441c5ef2d77cd2914da4c380b95e4

//    	imwrite(Nombres[A], Modificados[A]);
	waitKey();
}

