#ifndef COLORCONSTANCY_H
#define COLORCONSTANCY_H

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "iostream"
#include "cstdlib"
#include "math.h"

using namespace cv;
using namespace std;

class ColorConstancy{
    // Se crea la clase 'ColorConstancy'
    private: // Atributos
        Mat Original, Modificada, Canales[3];

    private: //M�todos Privados
        Mat Color_Constancy(Mat, int, float, float, bool);
        void Media_Croma(Mat, string);
        void Mostrar_Imagen(Mat, string);
        void GraficarHistograma3D(Mat, string);

    public: // M�todos P�blicos
        ColorConstancy(Mat); // Constructor de la clase 'ColorConstancy'
        void White_patch(float, bool);
        void Gray_World(float, bool);
        void Shades_of_Gray(float, float, bool);
        void Gray_Edges(int, float, bool);
	void Guardar_Imagen(String);

};

//Declaraci�n del constructor con la im�gen a tratar como par�metro
ColorConstancy::ColorConstancy(Mat _Original){
    // Constructor de la clase 'ColorConstancy'
    Original = _Original;
}

//M�todo para aplicar el algoritmo de constancia de color White Patch,
//con 2 argumentos: _Sigma para el filtro gaussiano y _Modificado para 
//seleccionar o no la aplicaci�n del m�todo por segmentos.
void ColorConstancy::White_patch( float _Sigma, bool _Modificado){
    split(Original, Canales);
    for (int i=0; i<=2; i++){
        Canales[i] = Color_Constancy(Canales[i], 0, 0, _Sigma, _Modificado);
    }
    merge(Canales, 3, Modificada);
    Media_Croma(Original, "Imagen original");
    Media_Croma(Modificada, "Imagen modificada");
    Mostrar_Imagen(Original, "Imagen original");
    Mostrar_Imagen(Modificada, "Imagen modificada");
    GraficarHistograma3D(Original, "Hist Imagen original");
    GraficarHistograma3D(Modificada, "Hist Imagen modificada");
}

//M�todo para aplicar el algoritmo de constancia de color Gray World,
//con 2 argumentos: _Sigma para el filtro gaussiano y _Modificado para 
//seleccionar o no la aplicaci�n del m�todo por segmentos.
void ColorConstancy::Gray_World( float _Sigma, bool _Modificado){
    split(Original, Canales);
    for (int i=0; i<=2; i++){
        Canales[i] = Color_Constancy(Canales[i], 0, 1, _Sigma, _Modificado);
    }
    merge(Canales, 3, Modificada);
    Media_Croma(Original, "Imagen original");
    Media_Croma(Modificada, "Imagen modificada");
    Mostrar_Imagen(Original, "Imagen original");
    Mostrar_Imagen(Modificada, "Imagen modificada");
    GraficarHistograma3D(Original, "Hist Imagen original");
    GraficarHistograma3D(Modificada, "Hist Imagen modificada");
}

//M�todo para aplicar el algoritmo de constancia de color Shades of Gray,
//con 3 argumentos: _Minkowski que es la norma  _Sigma para el filtro gaussiano y _Modificado para 
//seleccionar o no la aplicaci�n del m�todo por segmentos.
void ColorConstancy::Shades_of_Gray( float _Minkowski, float _Sigma, bool _Modificado){
    split(Original, Canales);
    for (int i=0; i<=2; i++){
        Canales[i] = Color_Constancy(Canales[i], 0, _Minkowski, _Sigma, _Modificado);
    }
    merge(Canales, 3, Modificada);
    Media_Croma(Original, "Imagen original");
    Media_Croma(Modificada, "Imagen modificada");
    Mostrar_Imagen(Original, "Imagen original");
    Mostrar_Imagen(Modificada, "Imagen modificada");
    GraficarHistograma3D(Original, "Hist Imagen original");
    GraficarHistograma3D(Modificada, "Hist Imagen modificada");
}

//M�todo para aplicar el algoritmo de constancia de color Shades of Gray,
//con 3 argumentos: _Edges paa elegir la primera (1) o segunda (2) derivada  
//_Sigma para el filtro gaussiano y 
//_Modificado para seleccionar o no la aplicaci�n del m�todo por segmentos.
void ColorConstancy::Gray_Edges( int _Edges, float _Sigma, bool _Modificado){
    if(_Edges==1 || _Edges==2){
        split(Original, Canales);
        for (int i=0; i<=2; i++){
            Canales[i] = Color_Constancy(Canales[i], _Edges, 1, _Sigma, _Modificado);
        }
        merge(Canales, 3, Modificada);
        Media_Croma(Original, "Imagen original");
        Media_Croma(Modificada, "Imagen modificada");
        Mostrar_Imagen(Original, "Imagen original");
        Mostrar_Imagen(Modificada, "Imagen modificada");
        GraficarHistograma3D(Original, "Hist Imagen original");
        GraficarHistograma3D(Modificada, "Hist Imagen modificada");
    }
}

//M�todo para mostrar la m�trica de constancia de color
void ColorConstancy::Media_Croma(Mat imagen, string name){
    Mat imagenCIELAB;
    float PromedioCroma = 0;
    cvtColor(imagen, imagenCIELAB, COLOR_RGB2Lab);
    for (int j=0; j<imagenCIELAB.rows; j++){
        uchar* data= imagenCIELAB.ptr<uchar>(j);
        for (int i=0; i<imagenCIELAB.cols; i++) {
            PromedioCroma += pow(pow(((double)data[(3*i)+1])-128,2)+pow(((double)data[(3*i)+2])-128,2), 0.5);
        }
    }
    PromedioCroma /= (imagenCIELAB.rows * imagenCIELAB.cols);
    cout << endl << "\tPromedio de la croma de " << name << " = " << PromedioCroma;
}

//M�todo Base para aplicar los algoritmos especificos de constancia de color
Mat ColorConstancy::Color_Constancy(Mat Source, int Edges, float Minkowski, float Sigma, bool Modificado){
    Mat Destiny;
    double minVal = 0, maxVal = 0;
    int HistSrc[256], HistDst[256];
    int Area = (Source.rows*Source.cols)/100;

    if (Sigma) GaussianBlur( Source, Source, Size(3,3), Sigma, Sigma, BORDER_DEFAULT );

    Mat GradienteX, GradienteY, imagenGradienteX, imagenGradienteY, imagenLaplace;
    switch (Edges){
        case 1:
            Sobel( Source, GradienteX, CV_16S, 1, 0, 3, 1, 0, BORDER_DEFAULT );
            convertScaleAbs( GradienteX, imagenGradienteX );
            Sobel( Source, GradienteY, CV_16S, 0, 1, 3, 1, 0, BORDER_DEFAULT );
            convertScaleAbs( GradienteY, imagenGradienteY );

            addWeighted( imagenGradienteX, 0.5, imagenGradienteY, 0.5, 0, Destiny );
            break;

        case 2:
            Laplacian( Source, imagenLaplace, CV_16S, 3, 1, 0, BORDER_DEFAULT );
            convertScaleAbs( imagenLaplace, Destiny );
            break;

        default:
            Destiny = Source.clone();
            break;
    }

    if (Minkowski){
        double Medida = mean(Destiny)[0];

        if (Minkowski != 1){
            Medida = 0;
            for (int j=0; j<Destiny.rows; j++) {
                uchar* data = Destiny.ptr<uchar>(j);
                for (int i=0; i<Destiny.cols; i++) {
                    Medida += pow(data[i], Minkowski);;
                }
            }
            Medida = pow(Medida/(Destiny.rows*Destiny.cols), 1/Minkowski);
        }

        if (!Modificado){
            minMaxLoc(Source,&minVal,0,0,0);
            Source.convertTo(Destiny, CV_8U, 255/((2*Medida)-minVal), 255/((2*Medida)-minVal)*(-minVal));
        }
        else {
            maxVal = 2*Medida;
            minMaxLoc(Source, &minVal, 0, 0, 0);
            for (int i=0; i<256; i++) {
                HistSrc[i] = 0;
            }
            for (int j=0; j<Source.rows; j++) {
                uchar* data = Source.ptr<uchar>(j);
                for (int i=0; i<Source.cols; i++) {
                    HistSrc[data[i]]++;
                }
            }

            Area = 0;
            for (int i=0; i<= maxVal; i++){
                Area += HistSrc[i];
            }
            Area /= 100;

            int Apuntador = (int)minVal;
            int Divisiones[3] = {63, 127, 191};
            int Acumulado = 0;
            int Paso = 1;
            while(1){
                Acumulado = Acumulado + HistSrc[Apuntador];

                switch (Paso) {
                    case 1:
                      if(Acumulado > 25*Area){
                            Divisiones[0] = Apuntador;
                            Paso = 2;
                      }
                      break;
                    case 2:
                      if(Acumulado > 50*Area){
                            Divisiones[1] = Apuntador;
                            Paso = 3;
                      }
                      break;
                    case 3:
                      if(Acumulado > 75*Area){
                            Divisiones[2] = Apuntador;
                            Paso = 0;
                      }
                      break;
                    default:
                        Paso = 0;
                      break;
                }

                if(Paso) Apuntador++;
                else break;
            }

            for (int j=0; j<Source.rows; j++) {
                uchar* data= Source.ptr<uchar>(j);
                uchar* data1= Destiny.ptr<uchar>(j);
                for (int i=0; i<Source.cols; i++) {
                    if (data[i] < minVal) data1[i] = 0;
                    else if (data[i] > maxVal) data1[i] = 255;
                    else if (data[i] <= Divisiones[0]){
                        data1[i] = (63*(data[i]-minVal))/(Divisiones[0]-minVal);
                    }else if (data[i]>Divisiones[0] && data[i]<=Divisiones[1]){
                        data1[i] = ((63*(data[i]-Divisiones[0]))/(Divisiones[1]-Divisiones[0]))+64;
                    }else if (data[i]>Divisiones[1] && data[i]<=Divisiones[2]){
                        data1[i] = ((63*(data[i]-Divisiones[1]))/(Divisiones[2]-Divisiones[1]))+128;
                    }else if (data[i] > Divisiones[2]){
                        data1[i] = ((63*(data[i]-Divisiones[2]))/(maxVal-Divisiones[2]))+192;
                    }
                }
            }
        }
    }
    else{
        minMaxLoc(Destiny, 0, &maxVal, 0, 0);
        minMaxLoc(Source, &minVal, 0, 0, 0);

        for (int i=0; i<256; i++) {
            HistSrc[i] = 0;
            HistDst[i] = 0;
        }
        for (int j=0; j<Source.rows; j++) {
            uchar* data = Source.ptr<uchar>(j);
            uchar* data1 = Destiny.ptr<uchar>(j);
            for (int i=0; i<Source.cols; i++) {
                HistSrc[data[i]]++;
                HistDst[data1[i]]++;
            }
        }

        int Acumulado = 0;
        while(1){
            Acumulado = Acumulado + HistDst[(int)maxVal];
            if(Acumulado < Area)
                maxVal = maxVal-1;
            else break;
        }

        if (!Modificado){
            Source.convertTo(Destiny, CV_8U, 255/(maxVal-minVal), 255/(maxVal-minVal)*(-minVal));
        }
        else{
            int Apuntador = (int)minVal;
            int Divisiones[3] = {63, 127, 191};
            Acumulado = 0;
            int Paso = 1;
            while(1){
                Acumulado = Acumulado + HistSrc[Apuntador];

                switch (Paso) {
                    case 1:
                      if(Acumulado > 25*Area){
                            Divisiones[0] = Apuntador;
                            Paso = 2;
                      }
                      break;
                    case 2:
                      if(Acumulado > 50*Area){
                            Divisiones[1] = Apuntador;
                            Paso = 3;
                      }
                      break;
                    case 3:
                      if(Acumulado > 75*Area){
                            Divisiones[2] = Apuntador;
                            Paso = 0;
                      }
                      break;
                    default:
                        Paso = 0;
                      break;
                }

                if(Paso) Apuntador++;
                else break;
            }

            for (int j=0; j<Source.rows; j++) {
                uchar* data= Source.ptr<uchar>(j);
                uchar* data1= Destiny.ptr<uchar>(j);
                for (int i=0; i<Source.cols; i++) {
                    if (data[i] < minVal) data1[i] = 0;
                    else if (data[i] > maxVal) data1[i] = 255;
                    else if (data[i] <= Divisiones[0]){
                        data1[i] = (63*(data[i]-minVal))/(Divisiones[0]-minVal);
                    }else if (data[i]>Divisiones[0] && data[i]<=Divisiones[1]){
                        data1[i] = ((63*(data[i]-Divisiones[0]))/(Divisiones[1]-Divisiones[0]))+64;
                    }else if (data[i]>Divisiones[1] && data[i]<=Divisiones[2]){
                        data1[i] = ((63*(data[i]-Divisiones[1]))/(Divisiones[2]-Divisiones[1]))+128;
                    }else if (data[i] > Divisiones[2]){
                        data1[i] = ((63*(data[i]-Divisiones[2]))/(maxVal-Divisiones[2]))+192;
                    }
                }
            }
        }
    }

    return Destiny;
}

//Metodo usado para mostrar la im�gen original y la modificada por los m�todos de cosntancia de color
void ColorConstancy::Mostrar_Imagen(Mat imagen, string name){
    namedWindow(name,0);
    imshow(name, imagen);
    waitKey(10);
}

//M�todo para almacenar la im�gen modificada en el directorio raiz del proyecto
void ColorConstancy::Guardar_Imagen(string _Name){
    imwrite(_Name,Modificada);
}

//M�todo usado para mostrar los histogramas de las im�genes original y modificada
void ColorConstancy::GraficarHistograma3D(Mat imagen, string name){
	int histSize = 256;
	float range[] = { 0, 255 };
    const float* histRange = { range };
	bool uniforme = true;
	bool acumulado = false;
	Mat b_hist, g_hist, r_hist;
    vector<Mat> componentes_bgr;

    split(imagen, componentes_bgr);

    // Calculamos los histogramas:
    calcHist( &componentes_bgr[0], 1, 0, Mat(), b_hist, 1, &histSize, &histRange, uniforme, acumulado );
    calcHist( &componentes_bgr[1], 1, 0, Mat(), g_hist, 1, &histSize, &histRange, uniforme, acumulado );
    calcHist( &componentes_bgr[2], 1, 0, Mat(), r_hist, 1, &histSize, &histRange, uniforme, acumulado );

    // Graficamos los histogramas para R, G y B:
    int hist_w = 512; int hist_h = 400;
    int bin_w = cvRound( (double) hist_w/histSize );
    Mat histImage( hist_h, hist_w, CV_8UC3, Scalar( 0,0,0));

    // Normalizamos la altura de los histogramas
    normalize(b_hist, b_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(g_hist, g_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );
    normalize(r_hist, r_hist, 0, histImage.rows, NORM_MINMAX, -1, Mat() );

    // Dibujamos los histogramas para cada canal
    for( int i = 1; i < histSize; i++ )
    {
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(b_hist.at<float>(i-1)) ) ,
                         Point( bin_w*(i), hist_h - cvRound(b_hist.at<float>(i)) ),
                         Scalar( 255, 0, 0), 1, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(g_hist.at<float>(i-1)) ) ,
                         Point( bin_w*(i), hist_h - cvRound(g_hist.at<float>(i)) ),
                         Scalar( 0, 255, 0), 1, 8, 0  );
        line( histImage, Point( bin_w*(i-1), hist_h - cvRound(r_hist.at<float>(i-1)) ) ,
                         Point( bin_w*(i), hist_h - cvRound(r_hist.at<float>(i)) ),
                         Scalar( 0, 0, 255), 1, 8, 0  );
    }
    namedWindow(name, 0 );
    imshow(name, histImage );

}

#endif // COLORCONSTANCY_H
